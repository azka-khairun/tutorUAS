def test_positive():
    self.assertEqual(konversi(1), "00000000000000000000000000000001")

def test_nol():
    self.assertEqual(konversi(0), "00000000000000000000000000000000")

def test_negative():
    self.assertEqual(konversi(-1), "11111111111111111111111111111111")
