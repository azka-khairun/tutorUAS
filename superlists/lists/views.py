from django.shortcuts import render

# Create your views here.
def konversi(n):
    n &= (2 << 31)-1
    formatStr = '{:0'+str(32)+'b}'
    ret = formatStr.format(int(n))
    return ret
